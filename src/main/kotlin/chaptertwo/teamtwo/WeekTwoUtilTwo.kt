package chaptertwo.teamtwo

import java.text.NumberFormat
import java.util.*

object WeekTwoUtilTwo {
    //TODO: buat function ekstention untuk membantu main function
    /*
    A. mengganti inputan harga dll dalam format rupiah
    B. merapihkan nama inputan barang (capital atau title case) *untuk poin ini tolong sepakati ke 3 group tidak boleh sama
    */
    fun Any.convertRupiah(): String {
        val localId = Locale("in", "ID")
        val formatter = NumberFormat.getCurrencyInstance(localId)
        val strFormat = formatter.format(this)
        return strFormat
    }

    fun String.titleCase(): String {
        return this.split(" ").joinToString(" ") {
            it.capitalize()
        }
    }
}
