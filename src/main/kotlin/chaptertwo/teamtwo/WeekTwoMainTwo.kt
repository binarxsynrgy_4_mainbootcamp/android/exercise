package chaptertwo.teamtwo

import chaptertwo.teamtwo.WeekTwoUtilTwo.convertRupiah
import chaptertwo.teamtwo.WeekTwoUtilTwo.titleCase
import java.time.LocalDate
import java.time.LocalTime

/**Team two
Anggota
Arief, Noverina, Bryan, Farhan
Tema = Toko Buku**/
class WeekTwoMainTwo {
    /* 1. ada inputan penjual berupa, Nama Barang, Harga, Jumlah
     * 2. ada inputan pembeli berupa, Nama dan jumlah yang dibeli*/
    // TODO : Buat fungsi inputanan penjual dan pembeli dengan tema toko buku

    fun penjual(bukuModels : ArrayList<Buku>){

        print("Berapa jumlah buku yang dimasukkan? : ")
        val total = Integer.valueOf(readLine())//membaca banyak jumlah buku yg mau di inputkan

        for (i in 1..total){//looping
            print("$i. Judul buku\t: ")
            val judul = readLine()//menerima inputan judul buku
            print("   Harga buku\t: ")
            val harga = Integer.valueOf(readLine())//menerima inputan harga buku
            print("   Jumlah buku\t: ")
            val jumlah = Integer.valueOf(readLine())//menerima inputan jumlah buku
            val buku = Buku(judul.toString(),harga,jumlah)//memasukkan ke dalam objek buku, yang kemudian dimasukkan ke dalam arraylist bukuModels
            bukuModels.add(buku)//data yang sudah diisi dimasukkan ke dalam arraylist bukuModels
        }
    }

    fun pembeli(bukuModels : ArrayList<Buku>, pembeliModels : ArrayList<Buku>) {
        println("kode")
        for (i in 0..bukuModels.size-1){
            /*menampilkan menu buku yang tersedia*/
            println("${i+1}. Judul buku\t: ${bukuModels[i].nama.titleCase()}") //ditambah satu supaya kode barang dimulai dari angka 1
            println("   Jumlah\t\t: ${bukuModels[i].jumlah}")
            println("   Harga\t\t: ${bukuModels[i].harga.convertRupiah()}")
        }

        var namaBuku = ""
        var hargaBuku = 0
        var jumlahBeli = 0
        var loop = 0
        //kondisi iterasi membeli buku untuk pembelian lebih dari satu jenis buku
        for (i in 0..loop) {
            /*membeli buku*/
            print("Masukkan kode barang : ")
            val kode = Integer.valueOf(readLine()) - 1 //dikurang satu supaya sesuai dengan index yang ada di dalam array
            if(kode in 0 until bukuModels.size){
                print("Jumlah beli : ")
                jumlahBeli = Integer.valueOf(readLine())
                if (jumlahBeli > bukuModels[kode].jumlah){ // test handle jumlah invalid
                    StockBarang(pembeliModels)
                    break
                }
                if (pembeliModels.size == 0) {
                    namaBuku = bukuModels[kode].nama
                    hargaBuku = bukuModels[kode].harga
                    pembeliModels.add(
                        Buku(
                            namaBuku,
                            hargaBuku,
                            jumlahBeli
                        )
                    ) //memasukkan ke dalam objek buku, yang kemudian dimasukkan ke dalam arraylist pembeliModels
                } else {
                    var cek = 0
                    for (j in 0 until pembeliModels.size) {
                        if (bukuModels[kode].nama == pembeliModels[j].nama) { //kalau dalam arrayList sudah ada buku yang sama, maka yg perlu ditambah hanya jumlah pembelian buku tersebut
                            pembeliModels[j].jumlah += jumlahBeli
                            cek = 1
                        }
                    }
                    if (cek == 0) {
                        namaBuku = bukuModels[kode].nama
                        hargaBuku = bukuModels[kode].harga
                        pembeliModels.add(
                            Buku(
                                namaBuku,
                                hargaBuku,
                                jumlahBeli
                            )
                        ) //memasukkan ke dalam objek buku, yang kemudian dimasukkan ke dalam arraylist pembeliModels
                    }
                }
            } else { // ErrorKode atau handle barang tidak ditemukan
                KodeError(pembeliModels)
                return pembeli(bukuModels,pembeliModels)
            }

            var loop2 = 0
            for (j in 0..loop2){
                print("\nApakah ada tambahan? [y/n] ")
                //kondisi untuk apakah ada tambahan belanjaan atau tidak
                when (readLine().toString().lowercase()) {
                    "y" -> {
                        loop++
                        /*menampilkan buku yang tersedia setelah dibeli*/
                        println("\n===Barang yang tersedia tinggal berikut : ===")
                        for (i in 0..bukuModels.size-1){
                            println("${i+1}. Judul buku\t: ${bukuModels[i].nama.titleCase()}")
                            println("   Jumlah\t\t: ${bukuModels[i].jumlah}")
                            println("   Harga\t\t: ${bukuModels[i].harga.convertRupiah()}")
                        }
                    }
                    "n" -> {}
                    else -> {
                        println("Anda salah input!!")
                        loop2++
                    }
                }
            }
        }
    }
}

fun main(){

    val bukuModels = ArrayList<Buku>()//inisialisasi arraylist buku untuk diisi beberapa objek buku
    val pembeliModels = ArrayList<Buku>()//inisialisasi arrayList pembeli untuk diisi beberapa objek buku yang dibeli
    val tokoBuku = WeekTwoMainTwo()//inisialisasi class utama dalam objek tokoBuku
    println("===Silahkan masukkan produk buku===")
    tokoBuku.penjual(bukuModels)//memanggil fungsi penjual dari objek tokoBuku

    println()

    println("===Silahkan pilih buku yang tersedia===")
    tokoBuku.pembeli(bukuModels, pembeliModels)//memanggil fungsi pembeli dari objek tokoBuku

    val total = total(pembeliModels)
    /*
     * memanggil fungsi total untuk menghitung total barang dan total harga
     * cara memanggil total barang : total.first
     * cara memanggil total harga : total.second
     */

    println("\nTotal belanjaan anda ${total.second.convertRupiah()}") //menampilkan total yang harus dibayar pembeli
    var loopMain = 0
    for (i in 0..loopMain) {
        print("Apakah anda ingin membayar? [y/n] ")
        //kondisi untuk pembeli, apakah ingin membayar atau tidak
        when (readLine().toString().lowercase()) {
            "y" -> {
                cetakStruk(pembeliModels, total) //fungsi cetak struk
            }
            "n" -> println("\nSampai jumpa kembali")
            else -> {
                println("\nAnda salah input!")
                loopMain++
            }
        }
    }

}

class Buku{
    var nama:String = ""
    var harga:Int = 0
    var jumlah:Int = 0

    constructor(_nama:String = "", _harga:Int = 0, _jumlah: Int = 0){
        this.nama = _nama
        this.harga = _harga
        this.jumlah = _jumlah
    }

}

fun total (pembeliModels: ArrayList<Buku>): Pair<Int, Int>
{
    var totalHarga = 0
    var totalBarang = 0

    for (i in 0 until pembeliModels.size) {
        totalHarga += (pembeliModels[i].harga*pembeliModels[i].jumlah) //harga buku yang telah dipilih dikali dengan jumlah buku tersebut
        totalBarang += pembeliModels[i].jumlah //total semua buku yang dibeli
    }
    return Pair(totalBarang, totalHarga) //mengembalikan 2 parameter yaitu total barang dan total harga
}

fun StockBarang(pembeliModels: ArrayList<Buku>){
    println("==========================================")
    print("Maaf Jumlah Yang Anda Masukkan Tidak Valid, \nMohon Input Kembali Jumlah\n")
    println("==========================================")
}

fun KodeError(pembeliModels: ArrayList<Buku>){
    println("===================================")
    print("Maaf Anda Menginput Kode Yang Salah\n")
    println("===================================")
}

fun cetakStruk (pembeliModels: ArrayList<Buku>, total: Pair<Int, Int>) {

    val date = LocalDate.now() //mengambil tanggal lokal saat belanja
    val time = LocalTime.now() //mengambil waktu lokal saat belanja
    println("===Toko Buku===")
    println("Selamat datang di toko buku kami")
    println("\n\n" +
            "Daftar belanja $date $time WIB:") //cetak tanggal dan waktu
    for (i in 0 until pembeliModels.size) {
        println("${pembeliModels[i].nama.titleCase()}\t| ${pembeliModels[i].jumlah}x |  ${pembeliModels[i].harga.convertRupiah()}")
    } //kondisi untuk cetak buku yang dibeli dengan memanggil data yang diambil dari arrayList pembeliModels
    println("Total\t${total.first} item  ${total.second.convertRupiah()}") //cetak total belanjaan dan total harga yang harus dibayar
    println("=================")
    println("\n\nSampai jumpa datang lagi")

}

//TODO: buat function sesuai kebutuhan untuk mengecek kondisi penjualan
/*fun kondisi dan looping
* 1. handle jika stok barang kurang dari permintaan pembelian
*  when/if (barang[i] < permintaan pembeli) {
*           back to inputan
*           println("maaf stok barang tidak terpenuhi")
*   } else {
*           next to pembelian
*   }
*
*
* 2. handle total pembelian
*   set jum = 0 and set jum=input.nextInt
*  for/when/if(i=0; i<jum; i++) {
*       total[i] = jumlahbarang[i] * harga[i]
*       total_bayar += total[i]
*       println( namabarang[i] + jumlahbarang[i] + kursIndonesia(total[i]))
*           i++ // utk next
*       }
*       println(" ")
*       println("Total Bayar : " + kursIndonesia.format(total_bayar)) // ini print total barang
* 3. handle barang yang dibeli tidak ada
*       for/when/if (barang[i]) {
*          barang[i] == barang[i]
*          next ato lolos }
*          else {
*               back to inputan(?)
*          }
*
* 4. buat extention function pada util per group untuk :
*   A. mengganti inputan harga dll dalam format rupiah
*       // begini kali buatnya hehe..
*       fun Any.kursIndonesia(): String {
        val localId = Locale("in", "ID")
        val formatter = NumberFormat.getCurrencyInstance(localId)
        val strFormat = formatter.format(this)
        return strFormat
}
*   B. merapihkan nama inputan barang (capital atau title case)
*       fun String.toCapitalize(): String {
            return this.split(" ").joinToString {
                it.capitalize()
            }
        }
* 5. print dengan foramt seperti pada struk belanja
* // coba doang
*
* println("========")
* println(" ")
* println("Sampai Jumpa Datang lagi")
* println(":D"
*
*
* */

//NOTE : kondisi dan looping saat mengecek dibuat beraneka ragam jangan sama dengan kelompok lain

