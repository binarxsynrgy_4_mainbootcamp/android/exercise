package chaptertwo.teamthree

import java.text.SimpleDateFormat
import java.util.*
import chaptertwo.teamthree.WeekTwoUtilThree.convertRupiah
import chaptertwo.teamthree.WeekTwoUtilThree.capitalizeWords

/**Team three
Anggota
Stheven, Hafid, Rasywan, Heru
Tema = Toko Baju**/
/* 1. ada inputan penjual berupa, Nama Barang, Harga, Jumlah
 * 2. ada inputan pembeli berupa, Nama dan jumlah yang dibeli*/
// TODO : Buat fungsi inputanan penjual dan pembeli dengan tema toko baju
fun main() {
    // array data untuk barang
    val toko = ArrayList<Barang>()
    // array data untuk pelanggan berbelanja
    val belanja = ArrayList<Belanja>()

    var flagBarang = false
    var flagBelanja = false

    // flag apabila ingin menambahkan barang
    var ulangBarang = "Y"
    // flag apabila ingin menambahkan belanja
    var ulangBelanja = "Y"

    //input untuk menambahkan barang
    println("======PENJUAL======")
    while (ulangBarang == "Y") {
        val read = Scanner(System.`in`)

        print("Nama barang yang dijual: ")
        val namaBarang = readLine()!!

        print("Harga $namaBarang: ")
        val hargaBarang = read.nextInt()

        print("Jumlah stok $namaBarang: ")
        val jumlahBarang = read.nextInt()

        val addItem = Barang(namaBarang, hargaBarang, jumlahBarang)
        toko.add(addItem)

        print("Apakah masih ada yang anda ingin jual? [Y/N] : ")
        val temp = readLine()
        ulangBarang = if (temp == "Y" || temp == "y") "Y" else "N"
    }
    // input dari pembeli
    println("======PELANGGAN======")
    while (ulangBelanja == "Y") {
        val read = Scanner(System.`in`)

        print("Nama barang yang dibeli: ")
        val namaBarangBelanja = readLine()!!


        print("Jumlah barang yang dibeli: ")
        val jumlahBelanja = read.nextInt()

       val checkInput = toko.find { it.barang == namaBarangBelanja }
        if (checkInput != null) {
            val addItem = Belanja(namaBarangBelanja, jumlahBelanja)
            belanja.add(addItem)
        } else {
            println("Maaf, Barang tidak tersedia Di toko kami")
        }

        print("Apakah masih ada yang anda ingin beli? [Y/N] : ")
        val temp = readLine()!!

        ulangBelanja = if (temp == "Y" || temp == "y") "Y" else "N"
    }
    println()
    println("====== BARANG YANG DIBELI ======")
    // check if nama barang in toko and belanja is same
    for (i in belanja) {
        for (j in toko) {
            if (i.barang == j.barang) {
                if (checkStock(i.jumlah, j.jumlah)) {
                    println("Nama barang: ${j.barang}")
                    println("Harga barang: ${j.harga.convertRupiah()}")
                    println("Jumlah barang: ${i.jumlah}")
                    println("Total harga: ${(i.jumlah * j.harga).convertRupiah()}")
                    flagBarang = true
                    flagBelanja = true
                    println()
                } else {
                    println("Stok ${i.barang} tidak mencukupi")
                    break
                }
            }
        }
    }
    if (flagBarang && flagBelanja) {
        println("Apakah anda ingin melanjutkan transaksi? [Y/N] : ")
        val temp = readLine()!!
        if (temp == "Y" || temp == "y") {
            invoice(toko, belanja)
        } else {
            println("Terima kasih, silahkan datang kembali \n:D")
        }
    } else {
        println("Mohon Periksa Kembali Inputan \n:D")
    }

}

// check stock penjualan barang lebih besar dari pembelian
private fun checkStock(jumlahBelanja: Int, jumlahBarang: Int): Boolean {
    return jumlahBelanja <= jumlahBarang
}


// generate invoice
private fun invoice(toko: ArrayList<Barang>, belanja: ArrayList<Belanja>) {
    println("======= TOKO KITA ========")
    println("Selamat datang di toko kita \n")
    // format dengan pattern yang telah ditentukan
    println("Daftar Belanja ${SimpleDateFormat("dd/MM/yyyy HH:mm").format(Date())} WIB")
    var total = 0
    var totalItem = 0
    // menampilkan daftar barang yang dibeli dan total harga dari seluruh barang yang dibeli
    for (i in 0 until toko.size) {
        for (j in 0 until belanja.size) {
            if (toko[i].barang == belanja[j].barang) {
                total += toko[i].harga * belanja[j].jumlah
                totalItem += belanja[j].jumlah
                println("${toko[i].barang.capitalizeWords()} | ${belanja[j].jumlah}X | ${(toko[i].harga * belanja[j].jumlah).convertRupiah()}")
            }
        }
    }
    println("Total $totalItem item  ${total.convertRupiah()}")
}



// model data barang
data class Belanja(val barang: String, val jumlah: Int)

// model data toko
data class Barang(val barang: String, val harga: Int, val jumlah: Int)


//TODO: buat function sesuai kebutuhan untuk mengecek kondisi penjualan
/*fun kondisi dan looping
* 1. handle jika stok barang kurang dari permintaan pembelian
* 2. handle total pembelian
* 3. handle barang yang dibeli tidak ada
* 4. buat extention function pada util per group untuk :
*   A. mengganti inputan harga dll dalam format rupiah
*   B. merapihkan nama inputan barang (capital atau title case)
* 5. print dengan foramt seperti pada struk belanja*/
//NOTE : kondisi dan looping saat mengecek dibuat beraneka ragam jangan sama dengan kelompok lain
//NOTE : kondisi dan looping saat mengecek dibuat beraneka ragam jangan sama dengan kelompok lain




