package chaptertwo.teamthree

import java.text.NumberFormat
import java.util.*

object WeekTwoUtilThree {
    //TODO: buat function ekstention untuk membantu main function
    /*
    A. mengganti inputan harga dll dalam format rupiah
    B. merapihkan nama inputan barang (capital atau title case) *untuk poin ini tolong sepakati ke 3 group tidak boleh sama
    */

    fun Number.convertRupiah(): String {
        // convert to rupiah format Rp.100,000
        val localeID = Locale("in", "ID")
        val formatRupiah = NumberFormat.getCurrencyInstance(localeID)
        return formatRupiah.format(this)
    }

    fun String.capitalizeWords(): String {
        return split(" ").joinToString(" ") { it.capitalize() }
    }

}
