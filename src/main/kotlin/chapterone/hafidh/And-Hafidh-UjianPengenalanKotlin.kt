package chapterone.hafidh

fun main() {

    println("Masukan Jumlah baris yg diinginkan : (jumlah baris 5 seperti output diclassroom)")
    val inputUser = readLine()!!.toInt()
    jamPasir(inputUser)
    println("=============================================================================")
    ketupat(inputUser)
    println("=============================================================================")
    customJ(inputUser)

}

private fun customJ(n: Int) {
    // looping segitiga atas
    for (i in n downTo 1) {
        // looping spasi 1,2,3,4
        for (space in 1..n - i) {
            print("  ")
        }
        for (j in 0 until (2 * i) - 1) { // lopping 1: 0 -> 8, lopping 2: 0 -> 6 (rumusnya: (2n-1-i))
            print("* ")
        }
        // print baris baru
        println()
    }
    // custom bawahnya
    for (l in 1..n) {
        // looping spasi mulai dari 5 -> 1
        for (m in (n + 1) - 1 downTo l) {
            print("  ")
        }
        // print bintang dari index ke 1 sampai 4
        for (o in 1 until n) {
            print("* ")
        }
        // print baris baru
        println()
    }

}


private fun ketupat(n: Int) {

    // looping segitiga atas
    for (i in 1..n) {
        // lopping spasi 1,2,3,4
        for (space in 1..n - i) {
            print("  ")
        }
        // print bintang sampai (2*i-1) cth: looping pertama 1, looping ke-2 0,1,2 dan seterusnya
        for (k in 0 until (i * 2) - 1) {
            print("* ")
        }
        println()
    }
    //segitiga bawah dari n sampai 1
    for (a in n downTo 1) {
        // looping spasi 1,2,3,4
        for (space in 1..n-a) {
            print("  ")
        }
        // looping bintang dengan rumus (2a-1)
        for (c in 0 until (a * 2) - 1) {
            print("* ")
        }
        // baris baru
        println()
    }
}


private fun jamPasir(n: Int) {
    // untuk segitiga atas

    // looping segitiga atas
    for (i in n downTo 1) {
        // looping spasi 1,2,3,4
        for (space in 1..n - i) {
            print("  ")

        }
        for (j in 0 until (2 * i) - 1) { // lopping 1: 0 -> 8, lopping 2: 0 -> 6 (rumusnya: (2n-1-i))
            print("* ")
        }
        // print baris baru
        println()
    }

    // untuk segitiga bawah
    for (k in 1..n) {
        // looping spasi
        for (space in (n - k) - 1 downTo 0) {
            print("  ")
        }
        // print bintang 1,3,5,7,9
        for (l in 0 until (k * 2) - 1) {
            print("* ")
        }
        println()
    }
}