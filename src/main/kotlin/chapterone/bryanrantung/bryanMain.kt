package chapterone.bryanrantung

fun main(args:Array<String>) {
    jamPasir()
    ketupat()
}

fun jamPasir(){
    val rows = 5 // set baris
    var inx = 0
    val soal3 = ("========== Jam Pasir ==========")

    /** Jam Pasir */
    println(soal3)
    // for reverse segitiga
    for (i in rows downTo 1) {
        // buat for untuk spasi baris 1-5
        for (space in 1..rows - i) {
            // print 2 spasi agar menyeimbangkan kiri dan kanan
            print("  ")
            // print("$space ")
        }
        // buat for untuk bentuk bintang segitiga
        for (segi in 1..9) {
            // 1-5
            if (segi in i until 2 * i){
                print("* ")
            }
            // 6-9
            if (segi in 1..i - 1){
                print("* ")
            }
        }
        // print perline perintah for
        println()
    }
    // for segitiga sempurna
    for (i in 1..rows) {
        // buat for untuk 2 spasi baris 1-5
        for (space in 1..rows - i) {
            print("  ")
        }
        // merubah nilai menurut
        while (inx !== 2 * i - 1) {
            print("* ")
            inx++
        }
        // print perline for dan kembalikan inx 0
        println()
        inx = 0
    }
    /** Akhir Jam Pasir */
}

fun ketupat(){
    val rows = 5 // set baris
    var inx = 0
    val soal2 = ("========== Ketupat ==========")

    /** Ketupat */
    // segitiga sempurna
    println(soal2)
    for (i in 1..rows) {
        // buat for untuk 2 spasi baris 1-5
        for (space in 1..rows - i) {
            print("  ")
        }
        // merubah nilai menurut
        while (inx !== 2 * i - 1) {
            print("* ")
            inx++
        }
        // print perline for dan kembalikan inx 0
        println()
        inx = 0
    }
    // membuat segitiga menghadap kebawah
    // buat for untuk loop menangani jumlah baris
    for (i in rows downTo 1) {
        // buat for untuk spasi baris 1-5
        for (space in 1..rows - i) {
            // print 2 spasi agar menyeimbangkan kiri dan kanan
            print("  ")
            // print("$space ")
        }
        // buat for untuk bentuk bintang segitiga
        for (segi in 1..9) {
            // 1-5
            if (segi in i until 2 * i){
                print("* ")
            }
            // 6-9
            if (segi in 1..i - 1){
                print("* ")
            }
        }
        // print perline perintah for
        println()
    }
    /** Akhir Ketupat */
}