package chapterone.bryanrantung

fun main(args: Array<String>) {

    val soal1 = ("============ Segitiga Costume J ============")
    val rows = 5 // set baris menjadi 5
    var inx = 0


    /** Segitiga costume huruf j */
    // membuat segitiga menghadap kebawah
    // buat for untuk loop menangani jumlah baris
    println(soal1)
    for (i in rows downTo 1) {
        // buat for untuk spasi baris 1-5
        for (space in 1..rows - i) {
            // print 2 spasi agar menyeimbangkan kiri dan kanan
            print("  ")
            // print("$space ")
        }
        // buat for untuk bentuk bintang segitiga
        for (segi in 1..9) {
            // 1-5
            if (segi in i until 2 * i){
                print("* ")
            }
            // 6-9
            if (segi in 1..i - 1){
                print("* ")
            }
        }
        // print perline perintah for
        println()
    }
    // for membuat jajar genjar
    for (baris in 1..5) {
        // for untuk spasi menurun
        for (space in 5 downTo baris){
            print("  ")
        }
        // while untuk jumlah dan posisi bintang
        while (inx != 4 * baris) {
            print("* ")
            inx++
        }
        // print for perline
        println()
    }
    /** Akhir Segitiga costume huruf j */
}