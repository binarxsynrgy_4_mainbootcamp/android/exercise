
fun main(){
    buatCustomJ()
    buatBelahKetupat()
    buatJamPasir()
}

fun buatCustomJ(){

    // membuat 5 baris
    var rows = 5

    // looping untuk membuat segitiga terbalik
    for(index in rows downTo 1){

        // looping untuk membuat space (ruang kosong tanpa ada bintang)
        for (space in 1..rows - index){
            print("  ")
        }

        // looping untuk membuat segitiga siku2 kiri
        for (leftStar in index until index * 2){
            print("* ")
        }

        // looping untuk membuat segitiga siku2 kanan
        for (rightStar in 0..index - 2){
            print("* ")
        }

        println()
    }

    // looping membuat jajar genjang
    for (index in rows downTo 1){

        // looping membuat space (ruang kosong tanpa segitiga)
        for (space in 0 until index){
            print("  ")
        }

        // looping membuat bintang
        for (star in 1..rows){
            print("* ")
        }
        println()
    }
}

fun buatBelahKetupat(){

    // membuat 5 baris
    var rows = 5

    //looping membuat segitiga atas
    for(index in 1..rows){

        for (space in 1..rows - index){
            print("  ")
        }

        // looping membuat bintang
        for (star in 0 until index * 2){
            if(star != index){
                print("* ")
            }
        }

        println()
    }

    //looping membuat segita bawah
    for(index in rows downTo 1){

        // looping membuat space
        for (space in 1..rows - index){
            print("  ")
        }

        // looping untuk membuat segitiga siku2 kiri
        for (leftStar in index until index * 2){
            print("* ")
        }

        // looping untuk membuat segitiga siku2 kanan
        for (rightStar in 0..index - 2){
            print("* ")
        }
        println()
    }
}

fun buatJamPasir(){

    // membuat 5 baris
    var rows = 5

    //looping membuat segita atas
    for(index in rows downTo 1){

        //looping membuat spasi
        for (space in 1..rows - index){
            print("  ")
        }

        //looping membuat segitiga siku2 kiri
        for (leftStar in index until index * 2){
            print("* ")
        }

        //looping membuat segitiga siku2 kanan
        for (rightStar in 0..index - 2){
            print("* ")
        }
        println()
    }

    //looping membuat segitiga bawah
    for(i in 1..rows){

        // looping membuat space
        for (space in 1..rows - i){
            print("  ")
        }

        // looping membuat star
        for (star in 0 until i * 2){
            if(star != i){
                print("* ")
            }
        }

        println()
    }
}
