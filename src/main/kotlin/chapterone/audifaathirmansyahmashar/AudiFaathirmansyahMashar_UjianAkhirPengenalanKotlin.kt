fun main() {
    segitigaCustomHurufJ()
    belahKetupat()
    jamPasir()
}

private const val BORDER_TOP: Int = 5

fun segitigaCustomHurufJ() {
    // Bagian Atas
    // Segitiga Terbalik
    for (i in 1..BORDER_TOP) {
        // Membentuk Space Membantu Pembentukan Segitiga Siku Siku Terbalik
        for (j in 1..i) {
            print("  ")
        }

        // Segitiga Siku Siku Terbalik
        for (j in i..BORDER_TOP) {
            print("* ")
        }

        // Segitiga Siku Siku
        for (k in i until BORDER_TOP) {
            print("* ")
        }
        println()
    }

    // Bagian Bawah
    // Jajar Genjang
    for (i in 1..BORDER_TOP) {
        // Space untuk membantu pembentukan Jajar Genjang
        for (j in i..BORDER_TOP+1) {
            print("  ")
        }

        // Membuat Kotak character asterix 5x5
        for (j in 1..BORDER_TOP) {
            print("* ")
        }
        println()
    }
}

fun belahKetupat() {
    // Bagian Atas
    // Segitiga Piramida
    for (i in 1..BORDER_TOP) {
        // Space untuk membantu pembentukan segitiga siku siku
        for (j in i..BORDER_TOP) {
            print("  ")
        }

        // Pembentukan segitiga siku siku from 1 to 5
        for (j in 1..i) {
            print("* ")
        }

        // Pembentukan segitiga siku siku from 1 to 4
        for (k in 1 until i) {
            print("* ")
        }
        println()
    }

    // Bagian Bawah
    // Segitiga Piramida Terbalik
    for (i in 1..BORDER_TOP) {
        // Space untuk membantu pembentukan segitiga siku siku terbalik
        for (j in 1..i) {
            print("  ")
        }

        // Membentuk segitiga siku siku terbalik 5 to 1
        for (j in i..BORDER_TOP) {
            print("* ")
        }

        // Membentuk segitiga siku siku terbalik 4 to 1
        for (k in i until BORDER_TOP) {
            print("* ")
        }
        println()
    }
}

fun jamPasir() {
    // Bagian Atas
    // Segitiga Piramida Terbalik
    for (i in 1..BORDER_TOP) {
        // Space untuk membantu pembentukan segitiga siku siku terbalik
        for (j in 1..i) {
            print("  ")
        }

        // Segitiga siku siku terbalik 5 to 1
        for (j in i..BORDER_TOP) {
            print("* ")
        }

        // Membentuk segitiga siku siku terbalik 4 to 1
        for (k in i until BORDER_TOP) {
            print("* ")
        }
        println()
    }

    // Bagian Bawah
    // Segitiga Piramida
    for (i in 1..BORDER_TOP) {
        // Space untuk membantu pembentukan segitiga siku siku
        for (j in i..BORDER_TOP) {
            print("  ")
        }

        // Membentuk segitiga siku siku terbalik 1 to 5
        for (j in 1..i) {
            print("* ")
        }

        // Membentuk segitiga siku siku terbalik 1 to 4
        for (k in 1 until i) {
            print("* ")
        }
        println()
    }
}