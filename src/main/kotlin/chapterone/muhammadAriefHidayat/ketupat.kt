/**
 * @author Muhammad Areif hidayat
 * menyusun bintang berbentuk Ketupat
 * */

fun main(){
    //deklarasi dan asign variable spasi, bintang
    var mSpasi = 9
    var mBintang = 1

    //looping dari 1 sampai jumlah tingkat (10 tingkat) sesuai dengan gambar
    for (x in 1 .. 10){
        //percabangan when untuk menentukan bentuk seperti yang diinginkan
        when {
            //cabang pertama hingga dibawah tingkat 5 bentuk segitiga
            x > 5 ->{
                //panggil fungsi cetak ketupat
                cetakKetupatArief(mSpasi,mBintang)
                mSpasi+=2 // menggunakan operator untuk menambah jumlah  2 spasi
                mBintang-=2 // menggunakan operator untuk mengurangi jumlah  2 bintang
            }

            // pilihan ketika tingkat 5 ditengah bentuk bintang dan spasi dengan jumlah bintang 9
            x == 5 -> {
                //panggil fungsi cetak ketupat
                cetakKetupatArief(mSpasi,mBintang)
            }

            else -> {
                //panggil fungsi cetak ketupat
                cetakKetupatArief(mSpasi,mBintang)
                mSpasi-=2  // menggunakan operator untuk mengurangi jumlah  2 spasi
                mBintang+=2 // menggunakan operator untuk menambah jumlah  2 bintang
            }
        }
    }
}

// fungsi cetak Ketupat parameter spasi dan bintang
fun cetakKetupatArief(spasi:Int,bintang:Int) {
    //looping for untuk membentuk spasi dan bintang
    for (x in 1 .. spasi){
        // menuliskan spasi
        print(" ")
        // pengkondisian if untuk mencek apakah looping sudah di tahap akhir (x sama dengan jumlah spasi)
        if (x == spasi){
            // looping untuk menuliskan spasi dan bintang
            for (y in 1.. bintang){
                print("*")
                print(" ")
            }
        }
    }
    //menuliskan enter new line
    println("")
}
