/**
 * @author Muhammad Areif hidayat
 * menyusun bintang berbentuk Ketupat
 * */


fun main(){
    //deklarasi dan asign variable spasi, bintang
    var mSpasi = 1
    var mBintang = 9

    //looping dari 1 sampai jumlah tingkat (10 tingkat) sesuai dengan gambar
    for (x in 1 .. 10){
        //panggil fungsi segitiga J
        cetakSegitigaJArif(mSpasi,mBintang)
        //percabangan if else untuk menentukan bentuk seperti yang diinginkan
        if (x > 5){  //cabang pertama hingga dibawah tingkat 5 bentuk segitiga terbalik
            mSpasi -=2
        }else{
            mBintang -=2
            mSpasi +=2
        }
    }
}

fun cetakSegitigaJArif(spasi:Int,bintang:Int) {
    //looping for untuk membentuk spasi dan bintang
    for (x in 0 .. spasi){
        // menuliskan spasi
        print(" ")
        // pengkondisian if untuk mencek apakah looping sudah di tahap akhir (x sama dengan jumlah spasi)
        // dan jumlah bitang dibawah 0
        if (x == spasi && bintang < 0){
           print("* * * *") // gak perlu looping karena bentuk statis dan pemborosan konsumsi memory

            // pengkondisian else if untuk mencek apakah looping sudah di tahap akhir (x sama dengan jumlah spasi)
            // dan jumlah bitang lebih dari 0
        }else if (x == spasi && bintang > 0){
            // looping untuk menuliskan spasi dan bintang
            for (y in 1 .. bintang){
                print("*")
                print(" ")
            }
        }
    }
    //menuliskan enter new line
    println()
}
