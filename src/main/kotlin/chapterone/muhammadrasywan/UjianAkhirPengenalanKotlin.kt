package chapterone.muhammadrasywan

/**
 * @author Muhamamad Rasywan
 * Membuat looping yang menyusun bintang berbentuk segetiga costume huruf j, ketupat dan jam pasir
 * Tekan Ctrl+Click pada function ilustrasi untuk melihat model gambar yang ditampilkan
 */


//deklarasi variable global agar bisa digunakan oleh semua function
val ROWS = 5
var K = 0

fun main() {
    println("\nSoal nomor 1\n=================")
    soalNomorSatu()
    println("\nSoal nomor 2\n=================")
    soalNomorDua()
    println("\nSoal nomor 3\n=================")
    soalNomorTiga()
}

fun soalNomorSatu () {
    // for ini akan menampilkan 3 segitiga dengan style yang berbeda-beda
    for (i in ROWS downTo 1) {
        // for untuk menampilkan space kosong berbentuk segitiga siku-siku
        ilustrasi1()
        for (space in 1..ROWS - i) {
            print("  ")
        }
        // for untuk menampilkan segitiga siku-siku terbalik dengan symbol *
        ilustrasi2()
        for (j in i..2 * i - 1) {
            print("* ")
        }
        // for untuk menampilkan segitiga siku-siku terbalik dengan symbol *
        ilustrasi3()
        for (j in 0..i - 2) {
            print("* ")
        }
        println()
    }
    //for ini akan menampilkan segitiga siku-siku dan jajar genjang
    for (i in 1..ROWS) {
        // for untuk menampilkan space kosong berbentuk segitiga siku-siku terbalik
        ilustrasi4()
        for (space in 1..ROWS - i) {
            print("  ")
        }
        // for untuk menampilkan jajar genjang dengan symbol *
        ilustrasi5()
        for (j in 0..ROWS - 2) {
            print("* ")
        }
        println()
    }
}

fun soalNomorDua () {
    // for ini akan menampilkan 3 segitiga dengan style yang berbeda-beda
    for (i in ROWS downTo 1) {
        // for untuk menampilkan space kosong berbentuk segitiga siku-siku
        ilustrasi1()
        for (space in 1..ROWS - i) {
            print("  ")
        }
        // for untuk menampilkan segitiga siku-siku terbalik dengan symbol *
        ilustrasi2()
        for (j in i..2 * i - 1) {
            print("* ")
        }
        // for untuk menampilkan segitiga siku-siku terbalik dengan symbol *
        ilustrasi3()
        for (j in 0..i - 2) {
            print("* ")
        }
        println()
    }
    // for ini akan menampilkan 2 segitiga dengan style yang berbeda-beda
    for (i in 1..ROWS) {
        // for untuk menampilkan space kosong berbentuk segitiga siku-siku terbalik
        ilustrasi4()
        for (space in 1..ROWS - i) {
            print("  ")
        }
        // for untuk menampilkan segitiga sama kaki dengan symbol *
        ilustrasi6()
        while (K != 2 * i - 1) {
            print("* ")
            K++
        }
        println()
        K = 0
    }
}

fun soalNomorTiga() {
    // for ini akan menampilkan 2 segitiga dengan style yang berbeda-beda
    for (i in 1..ROWS) {
        // for untuk menampilkan space kosong berbentuk segitiga siku-siku terbalik
        ilustrasi4()
        for (space in 1..ROWS - i) {
            print("  ")
        }
        // for untuk menampilkan segitiga sama kaki dengan symbol *
        ilustrasi6()
        while (K != 2 * i - 1) {
            print("* ")
            K++
        }
        println()
        K = 0
    }
    // for ini akan menampilkan 3 segitiga dengan style yang berbeda-beda
    for (i in ROWS downTo 1) {
        // for untuk menampilkan space kosong berbentuk segitiga siku-siku
        ilustrasi1()
        for (space in 1..ROWS - i) {
            print("  ")
        }
        // for untuk menampilkan segitiga siku-siku terbalik dengan symbol *
        ilustrasi2()
        for (j in i..2 * i - 1) {
            print("* ")
        }
        // for untuk menampilkan segitiga siku-siku terbalik dengan symbol *
        ilustrasi3()
        for (j in 0..i - 2) {
            print("* ")
        }
        println()
    }
}

fun ilustrasi1 () {
    //  |\
    //  | \
    //  |  \
    //  |___\
}

fun ilustrasi2 () {
    // * * * * *
    //   * * * *
    //     * * *
    //       * *
    //         *
}

fun ilustrasi3 () {
    // * * * *
    // * * *
    // * *
    // *
}

fun ilustrasi4 () {
    // _____
    // |   /
    // |  /
    // | /
    // |/
}

fun ilustrasi5 () {
    //         * * * *
    //       * * * *
    //     * * * *
    //   * * * *
    // * * * *
}

fun ilustrasi6 () {
    //         *
    //       * * *
    //     * * * * *
    //   * * * * * * *
    // * * * * * * * * *
}