package chapterone.`Noverina-Rahmaniyanti`

fun main() {
    jamPasir()
    ketupat()
}

fun jamPasir() {
    //variabel spasi menampung jumlah spasi hasil perulangan
    var spasi: String
    //variabel bintang menampung jumlah bintang hasil perulangan
    var bintang: String
    //variabel gabungan menampung jumlah spasi ditambah dengan jumlah bintang
    var gabungan: String
    //variabel loop adalah jumlah perulangan untuk bentuk segitiga
    val loop = 5
    //variabel spasiLoop untuk perulangan nilai spasi, hasil perulangan ditampung dalam variabel spasi
    val spasiLoop = "  "
    //variabel bintangLoop untuk perulangan nilai bintang, hasil perulangan ditampung dalam variabel bintang
    val bintangLoop = "* "
    //variabel nilaiBintangSegitigaTerbalik adalah nilai awal perulangan bintang untuk membentuk segitiga terbalik
    var nilaiBintangSegitigaTerbalik = 9
    //variabel nilaiBintangSegitigaMutable adalah nilai awal perulangan bintang untuk membentuk segitiga
    var nilaiBintangSegitigaMutable = loop

    //perulangan untuk membentuk segitiga terbalik
    /*
    * Dilakukan perulangan dari nilai i = 1 sampai i
    * bernilai sama dengan variabel loop.
    */
    for(i in 1.rangeTo(loop)) {
        spasi = "" //nilai awal spasi pada tiap perulangan
        bintang = "" //nilai awal bintang pada tiap perulangan

        /*
        * Dilakukan perulangan untuk jumlah bintang
        * di tiap baris perulangan utama. Perulangan
        * jumalah bintang dilakukan pada saat j yang
        * benilai sama dengan i sampai j bernilai sama
        * dengan variabel nilaiBintangSegitigaTerbalik. Di dalam
        * perulangan bintang ini, terdapat perintah untuk
        * mengisi nilai variabel bintang dengan bintangLoop
        * (bintang = bintang + bintangLoop)
        */
        for(j in i.rangeTo(nilaiBintangSegitigaTerbalik)){
            bintang += bintangLoop
        }

        /*
        * Dilakukan perulangan untuk jumlah spasi
        * di tiap baris perulangan utama. Perulangan
        * jumalah spasi dilakukan pada saat k yang
        * benilai sama dengan i turun sampai k bernilai sama
        * dengan 2. Di dalam perulangan spasi ini, terdapat
        * perintah untuk mengisi nilai variabel spasi
        * dengan spasiLoop (spasi = spasi + spasiLoop)
        */
        for (k in i.downTo(2)) {
            spasi+=spasiLoop
        }

        //variabel gabungan berisi nilai variabel spasi dijumlah nilai variabel bintang
        gabungan = spasi + bintang

        //dalam tiap perulangan, nilai dari variabel nilaiBintangSegitigaTerbalik selalu dikurang 1
        nilaiBintangSegitigaTerbalik-=1

        //fungsi println(gabungan) adalah untuk mencetak nilai gabungan menambahkan "enter" pada tiap perulangan
        println(gabungan)
    }

    //perulangan untuk membentuk segitiga
    /*
    * Dilakukan perulangan dari i bernilai sama
    * dengan variabel loop sampai i bernilai 1.
    */
    for(i in loop.downTo(1)) {
        spasi = "" //nilai awal spasi pada tiap perulangan
        bintang = "" //nilai awal bintang pada tiap perulangan

        /*
        * Dilakukan perulangan untuk jumlah bintang
        * di tiap baris perulangan utama. Perulangan
        * jumalah bintang dilakukan pada saat j yang
        * benilai sama dengan i sampai j bernilai sama
        * dengan variabel nilaiBintangSegitigaMutable. Di dalam
        * perulangan bintang ini, terdapat perintah untuk
        * mengisi nilai variabel bintang dengan bintangLoop
        * (bintang = bintang + bintangLoop)
        */
        for(j in i.rangeTo(nilaiBintangSegitigaMutable)){
            bintang += bintangLoop
        }

        /*
        * Dilakukan perulangan untuk jumlah spasi
        * di tiap baris perulangan utama. Perulangan
        * jumalah spasi dilakukan pada saat k yang
        * benilai sama dengan i turun sampai k bernilai sama
        * dengan 2. Di dalam perulangan spasi ini, terdapat
        * perintah untuk mengisi nilai variabel spasi
        * dengan spasiLoop (spasi = spasi + spasiLoop)
        */
        for (k in i.downTo(2)) {
            spasi+=spasiLoop
        }

        //variabel gabungan berisi nilai variabel spasi dijumlah nilai variabel bintang
        gabungan = spasi + bintang

        //dalam tiap perulangan, nilai dari variabel nilaiBintangSegitigaMutable selalu dikurang 1
        nilaiBintangSegitigaMutable+=1

        //fungsi println(gabungan) adalah untuk mencetak nilai gabungan menambahkan "enter" pada tiap perulangan
        println(gabungan)
    }
}

fun ketupat() {
    //variabel spasi menampung jumlah spasi hasil perulangan
    var spasi: String
    //variabel bintang menampung jumlah bintang hasil perulangan
    var bintang: String
    //variabel gabungan menampung jumlah spasi ditambah dengan jumlah bintang
    var gabungan: String
    //variabel loop adalah jumlah perulangan untuk bentuk segitiga
    val loop = 5
    //variabel spasiLoop untuk perulangan nilai spasi, hasil perulangan ditampung dalam variabel spasi
    val spasiLoop = "  "
    //variabel bintangLoop untuk perulangan nilai bintang, hasil perulangan ditampung dalam variabel bintang
    val bintangLoop = "* "
    //variabel nilaiBintangSegitigaTerbalik adalah nilai awal perulangan bintang untuk membentuk segitiga terbalik
    var nilaiBintangSegitigaTerbalik = 9
    //variabel nilaiBintangSegitigaMutable adalah nilai awal perulangan bintang untuk membentuk segitiga
    var nilaiBintangSegitigaMutable = loop

    //perulangan untuk membentuk segitiga
    /*
    * Dilakukan perulangan dari i bernilai sama
    * dengan variabel loop sampai i bernilai 1.
    */
    for(i in loop.downTo(1)) {
        spasi = "" //nilai awal spasi pada tiap perulangan
        bintang = "" //nilai awal bintang pada tiap perulangan

        /*
        * Dilakukan perulangan untuk jumlah bintang
        * di tiap baris perulangan utama. Perulangan
        * jumalah bintang dilakukan pada saat j yang
        * benilai sama dengan i sampai j bernilai sama
        * dengan variabel nilaiBintangSegitigaMutable. Di dalam
        * perulangan bintang ini, terdapat perintah untuk
        * mengisi nilai variabel bintang dengan bintangLoop
        * (bintang = bintang + bintangLoop)
        */
        for(j in i.rangeTo(nilaiBintangSegitigaMutable)){
            bintang += bintangLoop
        }

        /*
        * Dilakukan perulangan untuk jumlah spasi
        * di tiap baris perulangan utama. Perulangan
        * jumalah spasi dilakukan pada saat k yang
        * benilai sama dengan i turun sampai k bernilai sama
        * dengan 2. Di dalam perulangan spasi ini, terdapat
        * perintah untuk mengisi nilai variabel spasi
        * dengan spasiLoop (spasi = spasi + spasiLoop)
        */
        for (k in i.downTo(2)) {
            spasi+=spasiLoop
        }

        //variabel gabungan berisi nilai variabel spasi dijumlah nilai variabel bintang
        gabungan = spasi + bintang

        //dalam tiap perulangan, nilai dari variabel nilaiBintangSegitigaMutable selalu dikurang 1
        nilaiBintangSegitigaMutable+=1

        //fungsi println(gabungan) adalah untuk mencetak nilai gabungan menambahkan "enter" pada tiap perulangan
        println(gabungan)
    }

    //perulangan untuk membentuk segitiga terbalik
    /*
    * Dilakukan perulangan dari nilai i = 1 sampai i
    * bernilai sama dengan variabel loop.
    */
    for(i in 1.rangeTo(loop)) {
        spasi = "" //nilai awal spasi pada tiap perulangan
        bintang = "" //nilai awal bintang pada tiap perulangan

        /*
        * Dilakukan perulangan untuk jumlah bintang
        * di tiap baris perulangan utama. Perulangan
        * jumalah bintang dilakukan pada saat j yang
        * benilai sama dengan i sampai j bernilai sama
        * dengan variabel nilaiBintangSegitigaTerbalik. Di dalam
        * perulangan bintang ini, terdapat perintah untuk
        * mengisi nilai variabel bintang dengan bintangLoop
        * (bintang = bintang + bintangLoop)
        */
        for(j in i.rangeTo(nilaiBintangSegitigaTerbalik)){
            bintang += bintangLoop
        }

        /*
        * Dilakukan perulangan untuk jumlah spasi
        * di tiap baris perulangan utama. Perulangan
        * jumalah spasi dilakukan pada saat k yang
        * benilai sama dengan i turun sampai k bernilai sama
        * dengan 2. Di dalam perulangan spasi ini, terdapat
        * perintah untuk mengisi nilai variabel spasi
        * dengan spasiLoop (spasi = spasi + spasiLoop)
        */
        for (k in i.downTo(2)) {
            spasi+=spasiLoop
        }

        //variabel gabungan berisi nilai variabel spasi dijumlah nilai variabel bintang
        gabungan = spasi + bintang

        //dalam tiap perulangan, nilai dari variabel nilaiBintangSegitigaTerbalik selalu dikurang 1
        nilaiBintangSegitigaTerbalik-=1

        //fungsi println(gabungan) adalah untuk mencetak nilai gabungan menambahkan "enter" pada tiap perulangan
        println(gabungan)
    }
}