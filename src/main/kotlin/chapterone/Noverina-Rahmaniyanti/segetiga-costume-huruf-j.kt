fun main(args: Array<String>) {
    //variabel spasiLoop untuk perulangan nilai spasi, hasil perulangan ditampung dalam variabel spasi
    val spasiLoop = "  "
    //variabel bintangLoop untuk perulangan nilai bintang, hasil perulangan ditampung dalam variabel bintang
    val bintangLoop = "* "
    //variabel spasi menampung jumlah spasi hasil perulangan
    var spasi: String
    //variabel bintang menampung jumlah bintang hasil perulangan
    var bintang: String
    //variabel gabungan menampung jumlah spasi ditambah dengan jumlah bintang
    var gabungan: String
    //variabel nilaiBintangSegitiga adalah nilai awal perulangan bintang untuk membentuk segitiga
    var nilaiBintangSegitiga = 9
    //variabel nilaiJajarGenjang adalah nilai perulangan bintang untuk membentuk jajargenjang
    val nilaiJajarGenjang = 4
    //variable nilaiSpasiJajarGenjang adalah nilai awal perulangan spasi untuk menyempurnakan bentuk jajargenjang
    var nilaiSpasiJajarGenjang = 1
    //variabel loop adalah jumlah perulangan untuk bentuk segitiga dan jajargenjang
    val loop = 5

    //perulangan untuk membentuk segitiga terbalik
    /*
    * Dilakukan perulangan dari nilai i = 1 sampai i
    * bernilai sama dengan variabel loop.
    */
    for(i in 1.rangeTo(loop)) {
        spasi = "" //nilai awal spasi pada tiap perulangan
        bintang = "" //nilai awal bintang pada tiap perulangan

        /*
        * Dilakukan perulangan untuk jumlah bintang
        * di tiap baris perulangan utama. Perulangan
        * jumalah bintang dilakukan pada saat j yang
        * benilai sama dengan i sampai j bernilai sama
        * dengan variabel nilaiBintangSegitigia. Di dalam
        * perulangan bintang ini, terdapat perintah untuk
        * mengisi nilai variabel bintang dengan bintangLoop
        * (bintang = bintang + bintangLoop)
        */
        for(j in i.rangeTo(nilaiBintangSegitiga)){
            bintang += bintangLoop
        }

        /*
        * Dilakukan perulangan untuk jumlah spasi
        * di tiap baris perulangan utama. Perulangan
        * jumalah spasi dilakukan pada saat k yang
        * benilai sama dengan i turun sampai k bernilai sama
        * dengan 2. Di dalam perulangan spasi ini, terdapat
        * perintah untuk mengisi nilai variabel spasi
        * dengan spasiLoop (spasi = spasi + spasiLoop)
        */
        for (k in i downTo(2)) {
            spasi+=spasiLoop
        }

        //variabel gabungan berisi nilai variabel spasi dijumlah nilai variabel bintang
        gabungan = spasi + bintang

        //dalam tiap perulangan, nilai dari variabel nilaiBintangSegitiga selalu dikurang 1
        nilaiBintangSegitiga-=1

        //fungsi println(gabungan) adalah untuk mencetak nilai gabungan menambahkan "enter" pada tiap perulangan
        println(gabungan)
    }

    //perulangan untuk membentuk jajargenjang
    /*
    * Dilakukan perulangan dari nilai i = 1 sampai i
    * bernilai sama dengan variabel loop.
    */
    for(i in 1.rangeTo(loop)){
        spasi = "" //nilai awal spasi pada tiap perulangan
        bintang = "" //nilai awal bintang pada tiap perulangan

        /*
        * Dilakukan perulangan untuk jumlah bintang
        * di tiap baris perulangan utama. Perulangan
        * jumalah bintang dilakukan pada saat j yang
        * benilai sama dengan i sampai j bernilai sama
        * dengan variabel nilaiJajarGenjang. Di dalam
        * perulangan bintang ini, terdapat perintah untuk
        * mengisi nilai variabel bintang dengan bintangLoop
        * (bintang = bintang + bintangLoop)
        */
        for(j in 1.rangeTo(nilaiJajarGenjang)){
            bintang += bintangLoop
        }

        /*
        * Dilakukan perulangan untuk jumlah spasi
        * di tiap baris perulangan utama. Perulangan
        * jumalah spasi dilakukan pada saat k yang
        * benilai sama dengan nilaiSpasiJajarGenjang sampai k bernilai sama
        * dengan nilaiJajarGenjang ditambah 1. Di dalam perulangan spasi ini, terdapat
        * perintah untuk mengisi nilai variabel spasi
        * dengan spasiLoop (spasi = spasi + spasiLoop)
        */
        for(k in nilaiSpasiJajarGenjang.rangeTo(nilaiJajarGenjang+1)){
            spasi+=spasiLoop
        }

        //variabel gabungan berisi nilai variabel spasi dijumlah nilai variabel bintang
        gabungan = spasi + bintang

        //dalam tiap perulangan, nilai dari variabel nilaiSpasiJajarGenjang selalu ditambah 1
        nilaiSpasiJajarGenjang+=1

        //fungsi println(gabungan) adalah untuk mencetak nilai gabungan menambahkan "enter" pada tiap perulangan
        println(gabungan)
    }
}