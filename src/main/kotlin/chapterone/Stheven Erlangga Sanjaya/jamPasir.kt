//fun main(args: Array<String>) {
//    val rows = 5
//    var k = 0
//
//    //membuat bagian atas jam dengan for loop
//    for (i in rows downTo 1) {
//        //rumus untuk membuat space kosong
//        for (space in 1..rows - i) {
//            print("  ")
//        }
//
//        //rumus untuk mengprint * (1 sampai 5)
//        for (j in i..2 * i - 1) {
//            print("* ")
//        }
//
//        //rumus untuk mengprint * (5 sampai 9)
//        for (j in 1..i - 1) {
//            print("* ")
//        }
//
//        println()
//    }
//    //membuat bagian bawah jam dengan for loop
//    for (i in 1..rows) {
//        //rumus untuk membuat space kosong
//        for (space in 1..rows - i) {
//            print("  ")
//        }
//        //rumus untuk mengprint * dengan kelipatan ganjil
//        while (k != 2 * i - 1) {
//            print("* ")
//            ++k
//        }
//        println()
//        k = 0
//    }
//}