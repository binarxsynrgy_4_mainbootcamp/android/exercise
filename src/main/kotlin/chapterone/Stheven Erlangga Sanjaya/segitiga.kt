//fun main(args: Array<String>) {
//    val rows = 5
//
//    //membuat bagian atas jam dengan for loop
//    for (i in rows downTo 1) {
//        //rumus untuk membuat space kosong
//        for (space in 1..rows - i) {
//            print("  ")
//        }
//
//        //rumus untuk mengprint j (1 sampai 5)
//        for (j in i..2 * i - 1) {
//            print("j ")
//        }
//
//        //rumus untuk mengprint j (5 sampai 9)
//        for (j in 1..i - 1) {
//            print("j ")
//        }
//        println()
//    }
//
//    //membuat bagian bawah segitiga dengan for loop
//    for (i in 1..rows) {
//        //membuat space kosong
//        for (space in 1..rows - i) {
//            print("  ")
//        }
//        //mengprint j
//        for(j in 1..4) {
//            print(" j")
//        }
//        println()
//    }
//}