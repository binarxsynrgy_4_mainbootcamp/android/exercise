package chapterone.heru

fun main(){

    //Segitiga costum karakter huruf "j"
    println("Kondisi Segitiga costum karakter huruf j ")
    // untuk menampilkan segitiga pyramid terbalik
    for (a in 0..4) { //  looping utk menentukan banyaknya baris
        for (b in 0..a) { // looping utk  spasi
            print(" ")
        }
        for (c in 4.downTo(a)) { // looping utk print setengah dari total * di tiap baris
            print("*")
        }
        for (d in 3.downTo(a)) { // looping utk print sisa dari total * di tiap baris
            print("*")
        }
        println()
    }
    // untuk menampilkan jajar genjang
    for(a in 0 .. 4){ // looping utk menentukan banyaknya baris
        for (b in 5.downTo(a)){ // looping utk  spasi
            print(" ")
        }
        for (d in 0..3) { // looping utk print total * tiap baris
            print("*")
        }
        println()
    }

    //Belah Ketupat
    println()
    println("Kondisi Belah Ketupat")
    // untuk menampilkan segitiga pyramid 
    for (a in 0..4) {  //  looping utk menentukan banyaknya baris
        for (b in 4.downTo(a)) { // looping utk spasi
            print(" ")
        }
        for (c in 0..4) {    // looping untuk print setengah dari total * di tiap baris
            print("*")
            if (c == a) {          // jika sudah memenuhi total baris maka looping berhenti
                break
            }
        }
        for (d in 1..a) {    //looping utk print sisa dari total * di tiap baris
            print("*")
        }
        println()
    }
    // untuk menampilkan segitiga pyramid terbalik
    for (a in 0..4) { //  looping utk menentukan banyaknya baris
        for (b in 0..a) { // looping utk spasi
            print(" ")
        }
        for (c in 4.downTo(a)) { // looping untuk print setengah dari total * di tiap baris
            print("*")
        }
        for (d in 3.downTo(a)) { // looping untuk print sisa dari total * di tiap baris
            print("*")
        }
        println()
    }

    //Jam Pasir      //hampir sama seperti belah ketupat tinggal dibalik
    println()
    println("Kondisi Jam Pasir")
     // untuk menampilkan segitiga pyramid terbalik
    for (a in 0..4) {
        for (b in 0..a) {
            print(" ")
        }
        for (c in 4.downTo(a)) {
            print("*")
        }
        for (d in 3.downTo(a)) {
            print("*")
        }
        println()
    }
    // untuk menampilkan segitiga pyramid 
    for (a in 0..4) {
        for (b in 4.downTo(a)) {
            print(" ")
        }
        for (c in 0..4) {
            print("*")
            if (c == a) {
                break
            }
        }
        for (d in 1..a) {
            print("*")
        }
        println()
    }

}
