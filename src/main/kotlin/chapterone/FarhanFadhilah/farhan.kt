package chapterone.FarhanFadhilah

var baris = 5

fun main(){
    picture1()
    println()
    picture2()
    println()
    picture3()
}

//picture 1
fun picture1(){

    for(i in baris downTo 1){// looping baris
        //value i = 5 diterasi 1,
        //value i = 4 di iterasi 2,
        //value i = 3 di iterasi 3,
        //value i = 2 di iterasi 4,
        //value i = 1 di iterasi 5
        for (spasi in 1..baris - i){
            print("  ")
            //membuat spasi tiap kolom (posisi kiri). jadi baris 1 itu spasi nya null (karena tidak memenuhi dalam for),  baris 2 memiliki 1 spasi dan seterusnya, seperti yang dijelaskan komentar dibawah ini
            /*
            <value variable spasi> <iterasi ke>
            null iterasi 1 (tidak ada value/spasi),
            1 iterasi 2 (ada 1 spasi),
            1,2 iterasi 3 (ada 2 spasi),
            1,2,3 iterasi 4 (ada 3 spasi),
            1,2,3,4 iterasi 5 (ada 4 spasi),
            */
        }

        for (j in i..i * 2 - 1){// loop membuat bintang sisi kiri segitiga
            print("* ")
            /*
             <value variable j> <iterasi ke>
               5,6,7,8,9 iterasi 1,
                 4,5,6,7 iterasi 2,
                   3,4,5 iterasi 3,
                     2,3 iterasi 4,
                       1 iterasi 5
            */
        }


        for (k in 0..i - 2){
            //menambah 4 bintang sisi kanan segitiga
            print("* ")
            /*
            <value variable j> <iterasi ke>
              0,1,2,3 iterasi 1 (ada 4 bintang),
              0,1,2 iterasi 2 (ada 3 bintang),
              0,1 iterasi 3 (ada 2 bintang),
              0 iterasi 4 (ada 1 bintang),
           */
        }

        println()//membuat baris baru
    }

    for (i in baris downTo 1){
        //value i = 5 diterasi 1,
        //value i = 4 di iterasi 2,
        //value i = 3 di iterasi 3,
        //value i = 2 di iterasi 4,
        //value i = 1 di iterasi 5
        for (spasi in 0..i-1){
            print("  ")
            //membuat spasi tiap kolom (posisi kiri). jadi baris 1 memiliki 5 spasi, baris 2 memiliki 4 spasi dan seterusnya, seperti yang dijelaskan komentar dibawah ini
            /*
            <value variable spasi> <iterasi ke>
            0,1,2,3,4 iterasi 1 (ada 5 spasi),
            0,1,2,3 iterasi 2 (ada 4 spasi),
            0,1,2 iterasi 3 (ada 3 spasi),
            0,1 iterasi 4 (ada 2 spasi),
            0 iterasi 5 (ada 1 spasi),
            */
        }

        for (j in 1..baris){
            print("* ")
            //membuat bintang
            /*
             <value variable j> <iterasi ke>
                      1,2,3,4,5 iterasi 1 (ada 5 bintang),
                    1,2,3,4,5 iterasi 2 (ada 5 bintang),
                  1,2,3,4,5 iterasi 3 (ada 5 bintang),
                1,2,3,4,5 iterasi 4 (ada 5 bintang),
              1,2,3,4,5 iterasi 5 (ada 5 bintang),
            */
        }
        println()//membuat baris baru
    }
}

fun picture2(){

    //segitiga atas
    for(i in 1..baris){
        //value i = 1 diterasi 1,
        //value i = 2 di iterasi 2,
        //value i = 3 di iterasi 3,
        //value i = 4 di iterasi 4,
        //value i = 5 di iterasi 5
        //segita atas
        for (spasi in 1..baris - i){
            print("  ")
            //membuat spasi tiap kolom (posisi kiri). jadi baris 1 itu spasi 4,  baris 2 memiliki 3 spasi dan seterusnya, seperti yang dijelaskan komentar dibawah ini
            /*
            <value variable spasi> <iterasi ke>
            1,2,3,4 iterasi 1 (ada 4 spasi),
            1,2,3 iterasi 2 (ada 3 spasi),
            1,2 iterasi 3 (ada 2 spasi),
            1 iterasi 4 (ada 1 spasi),
            null iterasi 5 (tidak ada value/spasi)
            */
        }

        for (k in 0..i* 2 - 1){
            /*
            looping untuk segitiga, output dalam bentuk segitiga siku, namun output diberikan spasi bertujuan merubah menjadi segitiga sama kaki
            penggunaan if dibawah digunakan untuk pengkondisian hanya mencetak output dari nilai kecil ke besar, berikut output yang diberikan :
            <value variable spasi> <iterasi ke>
                    0 iterasi 1 untuk parent for loop (ada 1 bintang),
                  013 iterasi 2 untuk parent for loop (ada 3 bintang),
                01245 iterasi 3 untuk parent for loop (ada 5 bintang),
              0123567 iterasi 4 untuk parent for loop (ada 7 bintang),
            012346789 iterasi 5 untuk parent for loop (ada 9 bintang),
            */
            if(k != i){
                print("* ")//penggunaan spasi setelah bintang supaya membentuk segitiga sama kaki
            }
        }
        println()//membuat baris baru
    }

    //segita bawah
    for(i in baris downTo 1){// looping baris
        //value i = 5 diterasi 1,
        //value i = 4 di iterasi 2,
        //value i = 3 di iterasi 3,
        //value i = 2 di iterasi 4,
        //value i = 1 di iterasi 5
        for (spasi in 1..baris - i){
            print("  ")
            //membuat spasi tiap kolom (posisi kiri). jadi baris 1 itu spasi nya null (karena tidak memenuhi dalam for),  baris 2 memiliki 1 spasi dan seterusnya, seperti yang dijelaskan komentar dibawah ini
            /*
            <value variable spasi> <iterasi ke>
            null iterasi 1 (tidak ada value/spasi),
            1 iterasi 2 (ada 1 spasi),
            1,2 iterasi 3 (ada 2 spasi),
            1,2,3 iterasi 4 (ada 3 spasi),
            1,2,3,4 iterasi 5 (ada 4 spasi),
            */
        }

        for (j in i..i * 2 - 1){// loop membuat bintang sisi kiri segitiga
            print("* ")
            /*
             <value variable j> <iterasi ke>
               5,6,7,8,9 iterasi 1,
                 4,5,6,7 iterasi 2,
                   3,4,5 iterasi 3,
                     2,3 iterasi 4,
                       1 iterasi 5
            */
        }

        for (k in 0..i - 2){
            //menambah 4 bintang sisi kanan segitiga
            print("* ")
            /*
            <value variable j> <iterasi ke>
              0,1,2,3 iterasi 1 (ada 4 bintang),
              0,1,2 iterasi 2 (ada 3 bintang),
              0,1 iterasi 3 (ada 2 bintang),
              0 iterasi 4 (ada 1 bintang),
           */
        }
        println()//membuat baris baru
    }
}

fun picture3(){

    //segita atas
    for(i in baris downTo 1){// looping baris
        //value i = 5 diterasi 1,
        //value i = 4 di iterasi 2,
        //value i = 3 di iterasi 3,
        //value i = 2 di iterasi 4,
        //value i = 1 di iterasi 5
        for (spasi in 1..baris - i){
            print("  ")
            //membuat spasi tiap kolom (posisi kiri). jadi baris 1 itu spasi nya null (karena tidak memenuhi dalam for),  baris 2 memiliki 1 spasi dan seterusnya, seperti yang dijelaskan komentar dibawah ini
            /*
            <value variable spasi> <iterasi ke>
            null iterasi 1 (tidak ada value/spasi),
            1 iterasi 2 (ada 1 spasi),
            1,2 iterasi 3 (ada 2 spasi),
            1,2,3 iterasi 4 (ada 3 spasi),
            1,2,3,4 iterasi 5 (ada 4 spasi),
            */
        }

        for (j in i..i * 2 - 1){// loop membuat bintang sisi kiri segitiga
            print("* ")
            /*
             <value variable j> <iterasi ke>
               5,6,7,8,9 iterasi 1,
                 4,5,6,7 iterasi 2,
                   3,4,5 iterasi 3,
                     2,3 iterasi 4,
                       1 iterasi 5
            */
        }

        for (k in 0..i - 2){
            //menambah 4 bintang sisi kanan segitiga
            print("* ")
            /*
            <value variable j> <iterasi ke>
              0,1,2,3 iterasi 1 (ada 4 bintang),
              0,1,2 iterasi 2 (ada 3 bintang),
              0,1 iterasi 3 (ada 2 bintang),
              0 iterasi 4 (ada 1 bintang),
           */
        }
        println()//membuat baris baru
    }

    //segitiga bawah
    for(i in 1..baris){
        //value i = 1 diterasi 1,
        //value i = 2 di iterasi 2,
        //value i = 3 di iterasi 3,
        //value i = 4 di iterasi 4,
        //value i = 5 di iterasi 5
        //segita atas
        for (spasi in 1..baris - i){
            print("  ")
            //membuat spasi tiap kolom (posisi kiri). jadi baris 1 itu spasi 4,  baris 2 memiliki 3 spasi dan seterusnya, seperti yang dijelaskan komentar dibawah ini
            /*
            <value variable spasi> <iterasi ke>
            1,2,3,4 iterasi 1 (ada 4 spasi),
            1,2,3 iterasi 2 (ada 3 spasi),
            1,2 iterasi 3 (ada 2 spasi),
            1 iterasi 4 (ada 1 spasi),
            null iterasi 5 (tidak ada value/spasi)
            */
        }

        for (k in 0..i* 2 - 1){
            /*
            looping untuk segitiga, output dalam bentuk segitiga siku, namun output diberikan spasi bertujuan merubah menjadi segitiga sama kaki
            penggunaan if dibawah digunakan untuk pengkondisian hanya mencetak output dari nilai kecil ke besar, berikut output yang diberikan
                    0 iterasi 1 untuk parent for loop (ada 1 bintang),
                  013 iterasi 2 untuk parent for loop (ada 3 bintang),
                01245 iterasi 3 untuk parent for loop (ada 5 bintang),
              0123567 iterasi 4 untuk parent for loop (ada 7 bintang),
            012346789 iterasi 5 untuk parent for loop (ada 9 bintang),
            */
            if(k != i){
                print("* ")//penggunaan spasi setelah bintang supaya membentuk segitiga sama kaki
            }
        }

        println()//membuat baris baru

    }
}