fun main() {
   sampleStruk()
}

fun sampleStruk(){
    println("===Toko Kucing===")
    println("Selamat datang ditoko\nkucing makmur")
    println("\n\nDaftar belanja 2022/4/2 19:00 WIB:")
    println("Royal Canin | 3x |  Rp.100,000")
    println("Wiskas      | 2x |  Rp. 20,000")
    println("Pasir 10L   | 1x |  Rp.100,000")
    println("Total       6 item  Rp.420,000")
    println("=================")
    println("\n\nSampai jumpa datang lagi")
    println(":D")
}